#include "CSVLogger.h"

CSVLogger::CSVLogger(QString name)
{
    m_file = new QFile( name );
}


CSVLogger::~CSVLogger( )
{
    if ( m_file->isOpen() ) {
        flush();
        m_file->close();
    }
    delete m_file;
}


bool CSVLogger::openLog( )
{
    if ( ! m_file->fileName().isEmpty() ) {
        if ( m_file->open( QFile::WriteOnly ) )
        {
            m_isActive = true;
            setDevice( m_file );
            return true;
        }
    }
    m_isActive = false;
    return false;
}

void CSVLogger::closeLog()
{
    flush();
    m_file->close();
    m_isActive = false;
}
