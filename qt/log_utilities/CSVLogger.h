#include <QtCore/QFile>
#include <QtCore/QString>
#include <QtCore/QTextStream>

class CSVLogger : public QTextStream {
public:
    CSVLogger( QString name = "log.csv" );
    ~CSVLogger( );
    bool openLog( );
    void closeLog( );

private:
    QFile *m_file;
    bool m_isActive;
};
