class YearSlider : public QSlider
{
public:

        YearSlider():QSlider(Qt::Horizontal)
        {
        };

        void paintEvent(QPaintEvent *e)
        {
                QSlider::paintEvent(e);

                QStyle *st = style();
                QPainter p(this);

                int v = this->minimum();

                QStyleOptionSlider slider;
                slider.initFrom(this);

                int len = st->pixelMetric(QStyle::PM_SliderLength, &slider, 
this);

                int available = 
st->pixelMetric(QStyle::PM_SliderSpaceAvailable, 
&slider, this);

                QRect r;
                p.drawText(rect(), Qt::TextDontPrint, QString::number(9999), 
&r);

                while (v<this->maximum())
                {
                        QString vs = QString::number(v);

                        int left = QStyle::sliderPositionFromValue(minimum(), 
maximum(), v, 
available) + len;
                        int left_next = 
QStyle::sliderPositionFromValue(minimum(), maximum(), 
v+tickInterval(), available);
                        
                        QPoint pos(left,rect().bottom());
                        int right = left+r.width();

                        if (right<rect().right() && right<left_next)
                                p.drawText(pos,vs);

                        v+=this->tickInterval();
                }

        };

};