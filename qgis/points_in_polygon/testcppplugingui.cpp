/***************************************************************************
 *   Copyright (C) 2003 by Tim Sutton                                      *
 *   tim@linfiniti.com                                                     *
 *                                                                         *
 *   This is a plugin generated from the QGIS plugin template              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 ***************************************************************************/

#include "CSVLogger.h"
#include "testcppplugingui.h"
#include "qgscontexthelp.h"

#include "qgsmapcanvas.h"
#include "qgsmaplayer.h"

// qt includes
#include <QtCore/QDebug>
#include <QtCore/QList>
#include <QtCore/QStringList>
#include <QtGui/QMessageBox>
#include <QElapsedTimer>

//standard includes

TestCppPluginGui::TestCppPluginGui( QgisInterface * theInterface, QWidget* parent, Qt::WFlags fl )
    : QDialog( parent, fl ), m_iface(theInterface)
{
    setupUi( this );
    fillComboBoxes( );
}

TestCppPluginGui::~TestCppPluginGui()
{
}

void TestCppPluginGui::fillComboBoxes()
{
    // clearing combo boxes before try to append current maplayers
    pointLayerCombo->clear( );
    polygonLayerCombo->clear( );

    QList<QgsMapLayer *> layers = m_iface->mapCanvas()->layers();
    for( int i = 0; i < layers.size( ); i++ )
    {
        QgsVectorLayer *layer = qobject_cast<QgsVectorLayer *>( layers.at( i ) );
        QGis::GeometryType ltype = layer->geometryType( );
        if ( QGis::Point == ltype )
        {
            pointLayerCombo->addItem( layer->name( ), layer->id( ) );
        }
        else if ( QGis::Polygon == ltype )
        {
            polygonLayerCombo->addItem( layer->name( ), layer->id( ) );
        }
    }
}
#include "spatialindex/SpatialIndex.h"
void TestCppPluginGui::on_buttonBox_accepted()
{
    QgsMapCanvas *canvas = m_iface->mapCanvas();
    int polyLayerId =  polygonLayerCombo->itemData( polygonLayerCombo->currentIndex( ) ).toInt( );
    int pointLayerId = pointLayerCombo->itemData( pointLayerCombo->currentIndex( ) ).toInt( );
    QgsVectorLayer *layerPoly = qobject_cast<QgsVectorLayer *>( canvas->layer( polyLayerId ) );
    QgsVectorLayer *layerPoint = qobject_cast<QgsVectorLayer *>( canvas->layer( pointLayerId ) );
    qDebug() << "ok button clicked";
    if ( layerPoly->isValid( ) && layerPoint->isValid( ) )
    {
        qDebug() << "both layers are valids";
        QgsVectorDataProvider *polyProvider = layerPoly->dataProvider( );
        QgsVectorDataProvider *pointProvider = layerPoint->dataProvider( );
        QgsAttributeList allAttrs = polyProvider->attributeIndexes( );
        polyProvider->select( allAttrs );
        QgsFieldMap fieldList = polyProvider->fields( );
        int index = polyProvider->fieldNameIndex( countField->text( ) );
        // if the field that stores the number of points in polygon doesn't
        // exists yet, create a new one and append in existing fields
        if ( index == -1 )
        {
            index = polyProvider->fieldCount( );
            QgsField field( countField->text( ), QVariant::Double, "real", 24, 15, "point count field" );
            fieldList.insert( index, field );
        }
        QgsSpatialIndex *spatialIndex = createIndex ( pointProvider );
        pointProvider->rewind( );
        pointProvider->select( );
        //
        QgsFeature polyFeat;
        QgsFeature pntFeat;
        QgsFeature outFeat;
        int count = 0;
        //
        polyProvider->rewind( );
        polyProvider->select( allAttrs );
        int invalid = 0;
        int i = 0;
        QElapsedTimer timer;
        CSVLogger csvLog("/home/dvaz/log.csv");
        csvLog.openLog();
        timer.start();
        while ( polyProvider->nextFeature( polyFeat ) )
        {
            timer.restart();
            i++;
            QgsGeometry *inGeom = polyFeat.geometry();
            //QgsAttributeMap atMap = polyFeat.attributeMap();
            //outFeat.setAttributeMap( atMap );
            //outFeat.setGeometry( (const QgsGeometry &) (*inGeom) );
            if ( !inGeom->isGeosValid() ) {
                invalid++;
                csvLog << "#invalid" << endl;
                continue;
            }
            count = 0;
            QList< QgsFeatureId > pointList = spatialIndex->intersects( inGeom->boundingBox( ) );
            foreach ( QgsFeatureId featId, pointList )
            {
                pointProvider->featureAtId(featId, pntFeat, true);
                QgsGeometry *tmpGeom = pntFeat.geometry();
                if ( inGeom->intersects( tmpGeom ) )
                {
                    count += 1;
                }
            }
            csvLog << inGeom->wkbType() << ";" << inGeom->length() << ";"
                   << inGeom->area() << ";" << polygonVertex( polyFeat ) << ";"
                   << pointList.size() << ";" << count << ";"
                   << timer.nsecsElapsed() << endl;
        }
        qDebug() << "total geometries" << i << "invalids geometries" << invalid;
    }
    accept( );
}


void TestCppPluginGui::on_buttonBox_rejected()
{
    reject();
}

void TestCppPluginGui::on_buttonBox_helpRequested()
{
    QgsContextHelp::run( context_id );
}



QgsSpatialIndex* createIndex( QgsVectorDataProvider *provider )
{
    QgsFeature feat;
    QgsSpatialIndex *index = new QgsSpatialIndex();
    provider->rewind();
    provider->select();
    while ( provider->nextFeature( feat ) )
    {
        index->insertFeature( feat );
    }
    return index;
}

quint64 polygonVertex( QgsFeature & feat )
{
    qint64 vertex_count = 0;
    QgsGeometry *geom = feat.geometry( );
    QgsMultiPolygon polygons;
    if ( geom->type( ) == QGis::Polygon )
    {
        if ( geom->isMultipart() )
        {
            polygons = QgsMultiPolygon( geom->asMultiPolygon() );
        }
        else {
          polygons.append( geom->asPolygon() );
        }
        for (int i=0 ; i< polygons.size(); i++)
        {
            QgsPolygon polygon(polygons.at(i));
            for (int j=0 ; j < polygon.size(); j++ )
            {
                QgsPolyline line( polygon.at(j) );
                vertex_count += line.size();
            }
        }
    }
    return vertex_count;
}

// # Convinience function to create a spatial index for input QgsVectorDataProvider
// def createIndex( provider ):
//     feat = QgsFeature()
//     index = QgsSpatialIndex()
//     fit = provider.getFeatures()
//     while fit.nextFeature( feat ):
//         index.insertFeature( feat )
//     return index



//         sRs = polyProvider.crs()
//         if QFile(self.outPath).exists():
//             if not QgsVectorFileWriter.deleteShapeFile(self.outPath):
//                 return
//
//         writer = QgsVectorFileWriter(self.outPath, self.encoding, fieldList,
//                                      polyProvider.geometryType(), sRs)
//
