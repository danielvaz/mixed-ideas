/***************************************************************************
 *   Copyright (C) 2003 by Tim Sutton                                      *
 *   tim@linfiniti.com                                                     *
 *                                                                         *
 *   This is a plugin generated from the QGIS plugin template              *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 ***************************************************************************/
#ifndef TestCppPluginGUI_H
#define TestCppPluginGUI_H

#include <QDialog>
#include <ui_testcpppluginguibase.h>

#include "qgisinterface.h"
#include "qgsfeature.h"
#include "qgsgeometry.h"
#include "qgsspatialindex.h"
#include "qgsvectorlayer.h"
#include "qgsvectordataprovider.h"

quint64 polygonVertex( QgsFeature & feat );
QgsSpatialIndex* createIndex( QgsVectorDataProvider *provider );

/**
@author Tim Sutton
*/
class TestCppPluginGui : public QDialog, private Ui::TestCppPluginGuiBase
{
    Q_OBJECT
  public:
    TestCppPluginGui( QgisInterface * theInterface, QWidget* parent = 0, Qt::WFlags fl = 0 );
    ~TestCppPluginGui();

  private:
    void fillComboBoxes();
    static const int context_id = 0;
    QgisInterface * m_iface;

  private slots:
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();
    void on_buttonBox_helpRequested();

};

#endif
