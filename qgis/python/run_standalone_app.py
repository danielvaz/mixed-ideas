# -*- coding: utf-8 -*-

from PyQt4.QtCore import *
from qgis.core import *

app = QgsApplication(sys.argv, False)

app.setPrefixPath('/usr/local')
app.setPluginPath('/usr/local/lib/qgis/plugins/')
app.initQgis()

print app.libraryPath()
print app.pluginPath()

# Now the environment is ready to run some standalone code