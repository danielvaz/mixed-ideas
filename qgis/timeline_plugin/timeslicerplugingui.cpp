#include "timeslicerplugingui.h"
#include "qgscontexthelp.h"

//qt includes

//standard includes

TimeSlicerPluginGui::TimeSlicerPluginGui( QgisInterface* theInterface, QWidget* parent, Qt::WFlags fl )
    : QDialog( parent, fl ), mIface(theInterface)
{
  mVerticalLayout = new QVBoxLayout(this);
  mVerticalLayout->setSpacing(6);
  mVerticalLayout->setContentsMargins(11, 11, 11, 11);
  mVerticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
  mTimelineWidget = new TimelineController(mIface, this);
  mVerticalLayout->addWidget(mTimelineWidget);
}

TimeSlicerPluginGui::~TimeSlicerPluginGui()
{
    delete mTimelineWidget;
    delete mVerticalLayout;
}


