#include <QDebug>

#include "TimelineController.h"
#include "ui_TimelineController.h"

TimelineController::TimelineController(QgisInterface *theInterface, QWidget *parent) :
    QWidget(parent), mIface(theInterface), mTimeGranularity(SECONDS), ui(new Ui::TimelineController)
{
    ui->setupUi(this);
    init();
}

TimelineController::TimelineController(QDateTime start, QDateTime end, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TimelineController),
    m_startPeriod(start),
    m_endPeriod(end)
{
    ui->setupUi(this);
    setPeriod(start, end);
    init();
}

TimelineController::~TimelineController()
{
    delete ui;
}

void TimelineController::init()
{
    fillDateTimeFormatComboBox();
    fillGranularityComboBox();
    fillLayerComboBox();
    connect(ui->lowerDate, SIGNAL(valueChanged(int)), this, SLOT(lowerDateChanged(int)));
    connect(ui->upperDate, SIGNAL(valueChanged(int)), this, SLOT(upperDateChanged(int)));
    connect(ui->layerComboBox, SIGNAL(activated(int)), this, SLOT(layerChanged(int)));
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(attachButtonClicked()));
    connect(this, SIGNAL(rangeDateChanged(QDateTime,QDateTime)), this, SLOT(rangeDateUpdated(QDateTime,QDateTime)));
    connect(ui->timeGranularityComboBox, SIGNAL(activated(int)), this, SLOT(granularityChanged(int)));
    connect(ui->datetimeFormat, SIGNAL(activated(QString)), this, SLOT(timeFormatChanged(QString)));
}

void TimelineController::setPeriod(QDateTime start, QDateTime end)
{
    m_startPeriod = start;
    m_endPeriod = end;
    //
    ui->lowerDatetime->setDateTimeRange(start, end);
    ui->lowerDatetime->setDateTime(start);
    //
    ui->upperDatetime->setDateTimeRange(start, end);
    ui->upperDatetime->setDateTime(end);
    //
    ui->lowerDate->setMinimum(start.toTime_t());
    ui->lowerDate->setMaximum(end.toTime_t());
    ui->lowerDate->setValue(start.toTime_t());
    ui->upperDate->setMinimum(start.toTime_t());
    ui->upperDate->setMaximum(end.toTime_t());
    ui->upperDate->setValue(end.toTime_t());
    setMinimumWidth(1000);
    setMinimumHeight(150);
}

void TimelineController::setMinimumUpperDate(QDateTime value)
{
    ui->upperDatetime->setMinimumDateTime(value);
}

void TimelineController::setMaximumUpperDate(QDateTime value)
{
    ui->upperDatetime->setMaximumDateTime(value);
}

void TimelineController::setMinimumLowerDate(QDateTime value)
{
    ui->lowerDatetime->setMinimumDateTime(value);
}

void TimelineController::setMaximumLowerDate(QDateTime value)
{
    ui->lowerDatetime->setMaximumDateTime(value);
}

void TimelineController::setTimeGranularity(TimelineController::TimeGranularity value)
{
    mTimeGranularity = value;
    int granularityValue;

    switch(mTimeGranularity) {
    case SECONDS:
        qDebug() << "seconds";
        granularityValue = 1;
        break;
    case MINUTES:
        qDebug() << "minutes";
        granularityValue = 60;
        break;
    case HOURS:
        qDebug() << "hours";
        granularityValue = 60 * 60;
        break;
    case DAYS:
        qDebug() << "days";
        granularityValue = 24 * 60 * 60;
        break;
    case WEEKS:
        qDebug() << "weeks";
        granularityValue = 7 * 24 * 60 * 60;
        break;
    case MONTHS:
        qDebug() << "months";
        granularityValue = 31 * 24 * 60 * 60;
        break;
    case YEARS:
        qDebug() << "years";
        granularityValue = 365 * 24 * 60 * 60;
        break;
    }
    ui->lowerDate->setTickInterval(granularityValue);
    ui->upperDate->setTickInterval(granularityValue);
    ui->lowerDate->setSingleStep(granularityValue);
    ui->upperDate->setSingleStep(granularityValue);
    ui->lowerDate->setPageStep(10 * granularityValue);
    ui->upperDate->setPageStep(10 * granularityValue);
}

TimelineController::TimeGranularity TimelineController::getTimeGranularity()
{
    return mTimeGranularity;
}

void TimelineController::upperDateChanged(int value)
{
    int lower = ui->lowerDate->value();
    if (value < lower) {
        value = lower;
        ui->upperDate->setValue(lower);
    }
    ui->upperDatetime->setDateTime(QDateTime::fromTime_t(value));
    emit rangeDateChanged(ui->lowerDatetime->dateTime(), ui->upperDatetime->dateTime());
}

void TimelineController::lowerDateChanged(int value)
{
    int upper = ui->upperDate->value();
    if (value > upper) {
        value = upper;
        ui->lowerDate->setValue(upper);
    }
    ui->lowerDatetime->setDateTime(QDateTime::fromTime_t(value));
    emit rangeDateChanged(ui->lowerDatetime->dateTime(), ui->upperDatetime->dateTime());
}

void TimelineController::layerChanged(int value)
{
    int layerId = ui->layerComboBox->itemData(value).toInt();
    ui->fieldComboBox->clear();
    if (layerId != -1) {
        QgsVectorLayer *layer = getVectorLayerById(layerId);
        QgsVectorDataProvider *provider = layer->dataProvider();
        QMap<QString, int> fields = provider->fieldNameMap();
        foreach(QString fieldName, fields.keys()){
            ui->fieldComboBox->addItem(fieldName, fields.value(fieldName));
        }
    }

}

void TimelineController::attachButtonClicked()
{
    int layerId = ui->layerComboBox->itemData(ui->layerComboBox->currentIndex()).toInt();
    int fieldId = ui->fieldComboBox->itemData(ui->fieldComboBox->currentIndex()).toInt();
    QgsVectorLayer *vLayer = getVectorLayerById(layerId);
    QgsVectorDataProvider *prov = vLayer->dataProvider();
    qDebug() << "attach clicked layer id" << layerId << "field id" << fieldId;
    int minValue = prov->minimumValue(fieldId).toInt();
    int maxValue = prov->maximumValue(fieldId).toInt();
    QDateTime sDate = QDateTime::fromTime_t(minValue);
    QDateTime eDate = QDateTime::fromTime_t(maxValue);
    qDebug() << "minimum" << minValue << sDate.toString("dd/MM/yyyy hh:mm:ss");
    qDebug() << "maximum" << maxValue << eDate.toString("dd/MM/yyyy hh:mm:ss");
    //
    setPeriod(sDate, eDate);
}

void TimelineController::rangeDateUpdated(QDateTime s, QDateTime e)
{
    int layerId = ui->layerComboBox->itemData(ui->layerComboBox->currentIndex()).toInt();
    QString fieldId = ui->fieldComboBox->currentText();
    QgsVectorLayer *vLayer = getVectorLayerById(layerId);
    // Make our first symbol and range...
    double myOpacity = 1;
    int myMin = s.toTime_t();
    int myMax = e.toTime_t();
    QString myLabel = "Group 1";
    QColor myColour = QColor("#00FF00");
    QgsSymbolV2 *mySymbol1 = QgsSymbolV2::defaultSymbol(vLayer->geometryType());
    mySymbol1->setColor(myColour);
    mySymbol1->setAlpha(myOpacity);
    QgsRendererRangeV2 myRange1( myMin, myMax, mySymbol1, myLabel);
    QgsRangeList myRangeList;
    myRangeList.append(myRange1);
    QgsGraduatedSymbolRendererV2 *myRenderer= new QgsGraduatedSymbolRendererV2("", myRangeList);
    myRenderer->setMode(QgsGraduatedSymbolRendererV2::Custom);
    myRenderer->setClassAttribute(fieldId);

    vLayer->setRendererV2(myRenderer);
    vLayer->triggerRepaint();
}

void TimelineController::granularityChanged(int value)
{
    qDebug() << "granularity" << value;
    setTimeGranularity((TimeGranularity) value);
}

void TimelineController::timeFormatChanged(QString value)
{
    if (value == "Custom") {
        qDebug() << "Should open a dialog allowing user to add a custom string datetime format.";
        qDebug() << "Not implemented yet";
    }
}

void TimelineController::fillLayerComboBox()
{
    // clearing combo boxes before try to append current maplayers
    ui->layerComboBox->clear();
    ui->layerComboBox->addItem("---------------", -1);

    QList<QgsMapLayer *> layers = mIface->mapCanvas()->layers();
    for( int i = 0; i < layers.size( ); i++ )
    {
        QgsVectorLayer *layer = qobject_cast<QgsVectorLayer *>( layers.at( i ) );
        ui->layerComboBox->addItem( layer->name( ), layer->id( ) );
    }
}

void TimelineController::fillGranularityComboBox()
{
    ui->timeGranularityComboBox->clear();
    ui->timeGranularityComboBox->addItem("Seconds", SECONDS);
    ui->timeGranularityComboBox->addItem("Minutes", MINUTES);
    ui->timeGranularityComboBox->addItem("Hours", HOURS);
    ui->timeGranularityComboBox->addItem("Days", DAYS);
    ui->timeGranularityComboBox->addItem("Weeks", WEEKS);
    ui->timeGranularityComboBox->addItem("Months", MONTHS);
    ui->timeGranularityComboBox->addItem("Years", YEARS);
}

void TimelineController::fillDateTimeFormatComboBox()
{
    ui->datetimeFormat->clear();
    ui->datetimeFormat->addItem("Seconds since epoch Jan-1970");
    ui->datetimeFormat->addItem("yyyy/MM/dd hh:mm:ss");
    ui->datetimeFormat->addItem("Custom");
}

QgsVectorLayer *TimelineController::getVectorLayerById(int id)
{
    return qobject_cast<QgsVectorLayer *>(mIface->mapCanvas()->layer(id));
}
