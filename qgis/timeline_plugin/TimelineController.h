#ifndef TIMELINECONTROLLER_H
#define TIMELINECONTROLLER_H

#include <QtGui>
#include <QtGui/QtGui>
#include <QWidget>
#include <QDateTime>

#include "qgsmapcanvas.h"
#include "qgsvectorlayer.h"
#include "qgsvectordataprovider.h"
#include "qgisinterface.h"
#include "qgsrendererv2.h"
#include "qgsgraduatedsymbolrendererv2.h"
#include "qgssymbolv2.h"
#include "qgsrendererv2.h"

namespace Ui {
class TimelineController;
}

class TimelineController : public QWidget
{
    Q_OBJECT
    Q_ENUMS(TimeGranularity)
    
public:
    enum TimeGranularity { SECONDS, MINUTES, HOURS, DAYS, WEEKS, MONTHS, YEARS };
    TimelineController(QgisInterface* theInterface, QWidget *parent = 0);
    TimelineController(QDateTime start, QDateTime end, QWidget *parent = 0);
    ~TimelineController();
    void init();
    void setPeriod(QDateTime start, QDateTime end);

    void setMinimumUpperDate(QDateTime value);
    void setMaximumUpperDate(QDateTime value);
    void setMinimumLowerDate(QDateTime value);
    void setMaximumLowerDate(QDateTime value);

    void setTimeGranularity(TimeGranularity value);
    TimeGranularity getTimeGranularity();

protected:
    void fillLayerComboBox();
    void fillGranularityComboBox();
    void fillDateTimeFormatComboBox();
    QgsVectorLayer *getVectorLayerById(int id);

protected slots:
    void upperDateChanged(int value);
    void lowerDateChanged(int value);
    void layerChanged(int value);
    void attachButtonClicked();
    void rangeDateUpdated(QDateTime s, QDateTime e);
    void granularityChanged(int value);
    void timeFormatChanged(QString value);

signals:
    void rangeDateChanged(QDateTime, QDateTime);
    
private:
    Ui::TimelineController *ui;
    QDateTime m_startPeriod;
    QDateTime m_endPeriod;
    QgisInterface *mIface;
    TimeGranularity mTimeGranularity;
};

#endif // TIMELINECONTROLLER_H
