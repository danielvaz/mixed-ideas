#ifndef TimeSlicerPluginGUI_H
#define TimeSlicerPluginGUI_H

#include <QDialog>
#include <QVBoxLayout>

#include "qgisinterface.h"

#include "TimelineController.h"

class TimeSlicerPluginGui : public QDialog
{
    Q_OBJECT
  public:
    TimeSlicerPluginGui( QgisInterface* theInterface, QWidget* parent = 0, Qt::WFlags fl = 0 );
    ~TimeSlicerPluginGui();
private:
    QVBoxLayout *mVerticalLayout;
    TimelineController *mTimelineWidget;
    QgisInterface *mIface;

};

#endif
